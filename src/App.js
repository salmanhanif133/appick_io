import React, { useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import Question from "./components/Question";

function App() {
  /* 

questions = [
  {id:1, type:"multi-choice", questionTitle: "What is 1 + 1?", options: [
    {id:1, choice:"1", isCorrect:false},
    {id:1, choice:"2", isCorrect:true},
    {id:1, choice:"3", isCorrect:false},
    {id:1, choice:"4", isCorrect:false},
  ]}
]
*/
  
  const [questionArray, setquestionsArray] = useState([
    {
      id: uuidv4(),
      type: "multi-choice",
      questionTitle: "",
      options: [
        { id: uuidv4(), choice: "", isCorrect: true },
        { id: uuidv4(), choice: "", isCorrect: false },
      ],
    },
  ]);
  console.table(questionArray)
  return (
    <>
      <div className="bg-gray-200 h-64"></div>
      <div className="-mt-32 w-full">
        <div className="flex mb-4 justify-between pl-16 pr-32">
          <p className="text-4xl text-gray-700">Create New Quiz</p>
          <div className="w-40 flex items-center justify-between">
            <button className="bg-black text-white px-4 py-1 rounded-md">
              Save
            </button>
            <button className="bg-black text-white px-5 py-1 rounded-md">
              Cancel
            </button>
          </div>
        </div>
        <div className="bg-white rounded-md shadow-lg p-8 mb-4 ml-16 mr-32">
          <p className="font-bold text-base mb-2">
            Questions will be typed here
          </p>
          <p className="text-sm">Correct Answer here here</p>
        </div>
        {questionArray.length >= 1 ? (
          questionArray.map((question) => {
            return (
              <Question
                key={question.id}
                {...question}
                questionArray={questionArray}
                setquestionsArray={setquestionsArray}
              />
            );
          })
        ) : (
          <p>Add question</p>
        )}
      </div>
    </>
  );
}

export default App;
