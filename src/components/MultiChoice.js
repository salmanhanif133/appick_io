import React from "react";

const MultiChoice = ({
  options,
  updateOptionChoice,
  toggleCorrect,
  removeChoice,
  addChoice,
}) => {
  console.table(options)
  return (
    <div className="w-5/6  pl-4">
      {options.map((option, index) => {
        return (
          <div key={option.id} className="mb-4 flex items-center border border-gray-200 rounded ">
            <input
              onChange={updateOptionChoice(option.id)}
              className="appearance-none block w-full text-gray-700 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 placeholder-gray-400"
              type="text"
              placeholder={`${!option.choice && `Option ${index + 1}`}`}
            />
            <svg
              className={`w-8 h-8 mr-2 ${
                option.isCorrect ? "text-green-400" : "text-gray-300 "
              }`}
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => toggleCorrect(option.id)}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
              ></path>
            </svg>
            <svg
              className="w-8 h-8 mr-4 text-gray-600"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => removeChoice(option.id)}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M6 18L18 6M6 6l12 12"
              ></path>
            </svg>
          </div>
        );
      })}
      <div>
        {options.length < 4 && (
          <p className="text-gray-600 underline text-sm" onClick={addChoice}>
            Add option
          </p>
        )}
      </div>
    </div>
  );
};

export default MultiChoice;
