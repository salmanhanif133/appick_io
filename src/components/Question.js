import React from "react";
import { v4 as uuidv4 } from 'uuid';

import MultiChoice from "./MultiChoice";

const Question = ({
  type,
  questionTitle,
  options,
  questionArray,
  setquestionsArray,
  id,
}) => {
  const currentQuestion = questionArray.find(q => q.id === id);
  const updateQuestionTitle = (index) => (e) => {
    let newArray = [...questionArray];
    newArray.forEach((item) => {
      if (item.id === index) {
        item.questionTitle = e.target.value;
      }
    });
    setquestionsArray(newArray);
  };
  const updateOptionChoice = (index) => (e) => {
    let newArray = [...questionArray];
    newArray.forEach((item) => {
      item.options.forEach((option) => {
        if (option.id === index) {
          option.choice = e.target.value;
        }
      });
    });
    setquestionsArray(newArray);
  };

  const addNewQuestion = () => {
    let newId = uuidv4();
    const newQuestion = [
      ...questionArray,
      {
        id: newId,
        type: "multi-choice",
        questionTitle: "",
        options: [
          { id: uuidv4(), choice: "", isCorrect: true },
          { id: uuidv4(), choice: "", isCorrect: false },
        ],
      },
    ];
    setquestionsArray(newQuestion);
  };

  const addChoice = () => {
    currentQuestion.options.push({ id: uuidv4(), choice: "", isCorrect: false });
    persistQuestions();
  };

  const removeQuestion = (id) => {
    const newArray = questionArray.filter((item) => item.id !== id);
    setquestionsArray(newArray);
  };

  const removeChoice = (optionId) => {
    console.log(optionId)
    currentQuestion.options = currentQuestion.options.filter(o => o.id !== optionId);
    persistQuestions();
  };

  const persistQuestions = () => {
    const newQuestionsArray = questionArray.map(q => {
      if (q.id === id) {
        return {
          ...currentQuestion
        }
      }
      return q;
    })
    setquestionsArray(newQuestionsArray);
  }
  const toggleCorrect = (optionId) => {
    let newArray = [...questionArray];
    newArray.forEach((item) => {
      if (item.id === id) {
        item.options.forEach((option) => {
          if (option.id === optionId) {
            option.isCorrect = !option.isCorrect;
          }
        });
      }
    });
    setquestionsArray(newArray);
  };

  const changeType = (e) => {
    const newArray = [...questionArray];
    newArray.forEach((q) => {
      if (q.id === id) {
        q.type = e.target.value;
      }
    });
    setquestionsArray(newArray);
  };

  return (
    <div className="flex ml-16 mr-16 justify-between">
      <div className="bg-white rounded-md shadow-lg p-4 mb-4 w-full">
        <div className="w-full">
          <input
            onChange={updateQuestionTitle(id)}
            className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 placeholder-black"
            type="text"
            placeholder={`${!questionTitle && "Question will be typed here"}`}
          />
        </div>
        <div className="flex justify-between items-start">
          <div className="inline-block relative w-64">
            <select
              className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-3 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
              value={type}
              onChange={changeType}
            >
              <option value="multi-choice">Multi-choice</option>
              <option value="paragraph">Paragraph</option>
              <option value="option3">Option 3</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
          {
            {
              "multi-choice": (
                <MultiChoice
                  updateOptionChoice={updateOptionChoice}
                  addChoice={addChoice}
                  removeChoice={removeChoice}
                  toggleCorrect={toggleCorrect}
                  options={options}
                />
              ),
              paragraph: (
                <div className="w-full">
                  <textarea
                    // onChange={updateQuestionTitle(id)}
                    className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 placeholder-black"
                    type="text"
                    placeholder={`${
                      !questionTitle && "Question will be typed here"
                    }`}
                  />
                </div>
              ),
            }[type]
          }
        </div>
      </div>
      <div className="ml-4">
        <div className="bg-white rounded-full shadow-lg text-center items-center mb-4">
          <svg
            className="w-12 h-12 p-2"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            onClick={() => {
              removeQuestion(id);
            }}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
            ></path>
          </svg>
        </div>
        <div className="bg-white rounded-full shadow-lg text-center items-center">
          <svg
            className="w-12 h-12 p-2"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            onClick={addNewQuestion}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
            ></path>
          </svg>
        </div>
      </div>
    </div>
  );
};
export default Question;
